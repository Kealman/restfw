var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

GLOBAL.appName = '<%= projectName %>';

var config = require('rfw-config').init('config', 'router');

var RouterObject = require('rfw-router').boot(config.get('router'));
var loadRouter = require('rfw-router').load;


var middlewareControllers = require("middleware/controllers");
var expressValidator = require('express-validator');

var Logger = require("lib/logger/logger");
var ConsoleTransport = require("lib/logger/LoggerConsoleTransport");


// ======= Logger =======

Logger.addTransport(new ConsoleTransport());
GLOBAL.logger = Logger;

var multer  = require('multer');
require("lib/util/extendedValidators")(expressValidator);

var app = express();

// view engine setup

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(expressValidator());
app.use(multer({ dest: './tmp/'}))


loadRouter.load(app, RouterObject.routerArray);
app.use(middlewareControllers.success);
app.use(middlewareControllers.error);



module.exports = app;
