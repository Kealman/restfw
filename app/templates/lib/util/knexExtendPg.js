/**
 * Created by Evgeniy on 01.10.14.
 */
var config = require('rfw-config'),
    _ = require("underscore"),
    Q = require("q");


var _extend = function (knex) {
    "use strict";

    //TODO: Сделать покрасивее расширение объекта
    knex.client.QueryBuilder.prototype.pagination = function (page, limit) {
        var _then = this;
        var _returnObject = {
            count: 0
        };

        limit = limit || config.get("crud:pagination:limit");
        var offset = !_.isUndefined(page) ? (page - 1) * limit : 0;

        return knex.transaction(function (trx) {
            return trx.raw("DECLARE curs CURSOR FOR " + _then.toString())
                .then(function () {
                    //TODO: Разобраться почему knex.raw не биндит значения
                    return trx.raw("MOVE FORWARD " + parseInt(offset) + " IN curs");
                })
                .then(function (res) {
                    _returnObject.count += res.rowCount;
                    return trx.raw("FETCH " + parseInt(limit) + " FROM curs");
                })
                .then(function (res) {
                    _returnObject.count += res.rowCount;
                    _returnObject.collection = res.rows;
                    return trx.raw("MOVE FORWARD ALL IN curs");
                })
                .then(function (res) {
                    _returnObject.count += res.rowCount;
                    return _returnObject;
                });
        });
    };

    knex.client.QueryBuilder.prototype.clearUpdate = function (obj) {
        _.each(obj, function (item, key) {
            if (_.isUndefined(item)) {
                delete obj[key];
            }
        });
        return this.update(obj);
    };



    knex.client.QueryBuilder.prototype.filter = function(conditions){
        var arr = ["name", "=", conditions.name];
        var self = this;
        conditions.forEach(function(item){
            if(item.length < 2){
                throw new Error("Internal array must be < 2 elements");
            }

            var sql;

            if(item.length === 2 && !_.isUndefined(item[1])){  //unsafe mode
                sql = item[0].replace("#?#",knex.raw(item[1]));
            } else if(_.isString(item[2]) && item[2].indexOf("#?#") && !_.isUndefined(item[3])){  //replace mod
                sql = item[0] +" "+ item[1]+" " + item[2].replace("#?#", "?");
            } else if(item.length === 3 && !_.isUndefined(item[2])){    //primitive mode
                sql = item[0] + " " + item[1] + " ?";
            }
            if(sql && (item[3] || item[2])){
                self.andWhere(knex.raw(sql, item[3] || item[2]));
            }else if(sql){
                self.andWhere(knex.raw(sql));
            }
        });

        return this;
    };

    knex.client.QueryBuilder.prototype.sorting = function(conditions){
        var self = this;

        conditions.forEach(function(item){
            if(item.length !== 2) {
                throw new Error("Internal array must be 2 elements");
            }
            if(!_.isUndefined(item[1])){
                self.orderBy(item[0], item[1]);
                self.haveSorted = true;
            }
        });

        return self;
    };

    knex.client.QueryBuilder.prototype.defaultOrderBy = function(field, sort){
        if(this.haveSorted) {
            return this;
        }
        this.orderBy(field, sort);

        return this;
    };

};



module.exports = _extend;
