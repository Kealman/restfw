/**
 * Created by Evgeniy on 04.06.14.
 */
var Errors = require("rfw-errors");
var config = require('rfw-config');
var debug = require('debug')('<%=_.slugify(projectName) %>');
/**
 * Создает ответ об успехе операции
 * @param {Object} req - Объект запроса
 * @param {Object} res - Объект для ответа
 */
exports.success = function (req, res, next) {
        return res.send(404);
};
/**
 * Создает ответ ошибки
 * @param {Error} err - Объект ошибки
 * @param {Object} req - Объект запроса
 * @param {Object} res - Объект для ответа
 */
exports.error = function (err, req, res, next) {
    logger.error(' Internal server error: ', err);
    return res.send(err.code, {
        error: err.type,
        message: err.message,
        params: err.params
    })
}
