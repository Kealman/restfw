/**
 * Created by Evgeniy on 09.06.14.
 */
var _ = require("underscore");
var validator = require("validator");

module.exports = function (validators) {

//    validators.validator.extend("gteZero", function (str) {  //Больше или равно нулю
//        return Number(str) >= 0;
//    });
//
    validators.validator.extend("containsArray", function (str, array) {
        return array.indexOf(str) >= 0
    });

    validators.validator.extend("intInArray", function (array) {
      array = array.split(',');
      var isInt = true;
      _.each(array, function (v) {
        isInt = (v % 1 === 0);
      });

      return isInt;
    });

    validators.validator.extend("isIntArray", function (array) {
        array = array.split(',');
        var flag = true;
        _.each(array, function (v) {
            if(v % 1 !== 0){
                 flag = false;
            }
        });

        return flag;
    });

    validators.validator.extend("isCountryArray", function (array) {
        array = array.split(',');
        var flag = true;
        _.each(array, function (v) {
            // none - для тех у кого нет страны
            if (!/^[A-Z]{3}$|^none$/.test(v)) {
                flag = false;
            }
        });

        return flag;
    });

    validators.validator.extend('isFilterDate', function(str) {
        var date = new Date(parseInt(str, 10));
        return validator.isDate(date);
    });

    validators.validator.extend('thisObject', function(str, obj) {
        return _.isObject(obj);
    });

//
//    validators.validator.extend("thisUndefined", function (str, test) {
//        return !test;
//    });
//
//    validators.validator.extend("thisNotUndefined", function (str, test) {
//        return !!test;
//    });


};


