var config = require('rfw-config'),
    _ = require("underscore");

var knex = require("knex")({
    client: "pg",
    connection: config.get("db:pg"),
    debug: true
});

require("lib/util/knexExtendPg")(knex);


module.exports = knex;
