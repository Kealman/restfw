/**
 * Created by Evgeniy on 19.08.14.
 */
var Logger = function(env){
    this.transports = [];
    this.env = env || "default";
};

Logger.prototype.addTransport = function(trasport){
    if(checkFunctions(trasport)){
        trasport.setEnv(this.env);
        this.transports.push(trasport);
    }
    else
        throw new Error("Transport should have method `log`");
};

Logger.prototype.info = function(message, meta){
    this.log("info", message, meta);
};

Logger.prototype.warning = function(message, meta){
     this.log("warning", message, meta);
}

Logger.prototype.error = function(message, meta){
    this.log("error", message, meta);
};

Logger.prototype.log = function(level, message, meta){

    this.transports.forEach(function(transport){
        transport.log(level, message, meta);
    })
};

var checkFunctions = function(object){
    if(!object.log && !(object.log instanceof Function)) return false;
    if(!object.setEnv && !(object.setEnv instanceof Function)) return false;

    return true;
}

module.exports = new Logger(process.env.NODE_ENV);


