/*global describe, beforeEach, it*/
'use strict';

var path = require('path');
var assert = require('yeoman-generator').assert;
var helpers = require('yeoman-generator').test;
var os = require('os');

describe('fbs-fw-generator:app', function () {
  before(function (done) {
    console.log(os.tmpdir());
    helpers.run(path.join(__dirname, '../app'))
      .inDir(path.join('/project/generators', './temp-test'))
      .withOptions({ 'skip-install': true })
      .withPrompt({
        projectName: 'yoManFack'
      })
      .on('end', done);
  });

  it('creates files', function () {
    assert.file([
      'package.json',
      '.editorconfig',
      '.jshintrc',
      'bin/www'
    ]);
  });
});
