'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var yosay = require('yosay');

var FbsFwGeneratorGenerator = yeoman.generators.Base.extend({
  initializing: function () {
    this.pkg = require('../package.json');
  },

  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the primo FbsFw generator!'
    ));

    var prompts = [{
      type: 'input',
      name: 'projectName',
      message: 'How name you facking project?',
      default: 'ShitProject'
    },
    {
      type: "checkbox",
      name: "features",
      message: 'What connection have?',
      choices: [
        {
          name: "PgSQL",
          value: "pgsql",
          checked: true
        },
        {
          name: "MySQL",
          value: 'mysql',
          checked: false
        }
      ]
    }
    ];

    this.prompt(prompts, function (props) {
      function hasFeature(feat) { return props.features.indexOf(feat) !== -1; }


      this.projectName = props.projectName;
      this.pgsql = hasFeature("pgsql");
      this.mysql = hasFeature("mysql");



      done();
    }.bind(this));
  },

  writing: {
    app: function () {
      //this.dest.mkdir('app');
      //this.dest.mkdir('app/templates');
      this.dest.mkdir("config");
      this.dest.mkdir("config/router");
      this.dest.mkdir("config/default");
      this.dest.mkdir("bin");
      this.dest.mkdir("middleware");
      this.dest.mkdir("routes");
      this.dest.mkdir("routes/controllers");
      this.dest.mkdir("routes/validators");
      this.dest.mkdir("lib");
      this.dest.mkdir("lib/util");
      this.dest.mkdir("lib/dbConnect");
      this.dest.mkdir("lib/logger");

      this.template('bin/www','bin/www');
      this.template('_package.json', 'package.json');
      this.template('middleware/_controllers.js', 'middleware/controllers.js');
      this.template('config/default/_auth.json', 'config/default/auth.json');
      this.template('app.js', 'app.js');


      this.src.copy('config/router/_hello.json', 'config/router/hello.json');
      this.src.copy('config/default/_db.json', 'config/default/db.json');
      this.src.copy('routes/controllers/_hello.js', 'routes/controllers/hello.js');
      this.src.copy('routes/validators/_helloValidator.js', 'routes/validators/helloValidator.js');
      this.src.copy('lib/logger/logger.js', 'lib/logger/logger.js');
      this.src.copy('lib/logger/loggerConsoleTransport.js', 'lib/logger/loggerConsoleTransport.js');
      this.src.copy('lib/util/extendedValidators.js', 'lib/util/extendedValidators.js');
      this.src.copy('_.gitignore', '.gitignore');

      if(this.pgsql){
        this.src.copy('lib/util/knexExtendPg.js', 'lib/util/knexExtendPg.js');
        this.src.copy('lib/dbConnect/_pg.js','lib/dbConnect/pg.js');
      }

      if(this.mysql){
        this.src.copy('lib/dbConnect/_mysql.js','lib/dbConnect/mysql.js');
      }
    },

    projectfiles: function () {
      this.src.copy('editorconfig', '.editorconfig');
      this.src.copy('jshintrc', '.jshintrc');
    }
  },

  end: function () {
    this.installDependencies();
  }
});

module.exports = FbsFwGeneratorGenerator;
