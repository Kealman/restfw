/**
 * Created by Evgeniy on 19.08.14.
 */
"use strict";

var _ = require("underscore");

var defaultOption = {
    default: {
        error: {
            showStack: true
        },
        info: {
            meta: true
        }
    }
};

var ConsoleTranspot = function (option) {
    this.options = _.defaults(option || {}, defaultOption);
    return this;
};

ConsoleTranspot.prototype.setEnv = function (env) {
    this.env = env;
};

ConsoleTranspot.prototype.log = function (level, message, meta) {
    if (!this.getOptionByEnv(this.env)[level]) {
        level = "info";
    }
    this[level](message, meta);
};

ConsoleTranspot.prototype.info = function (message, meta) {

    var msg = [getLogDate() + " - " + message];
    if (meta) {
        msg.push(meta);
    }

    console.log.apply(console, msg);
};

ConsoleTranspot.prototype.error = function (message, meta) {
    var option = this.getOptionByEnv(this.env).error;

    console.error(getLogDate() + "-" + message);
    if (option.showStack) {
        var err;

        if (meta.stack) {
            err = meta;
        }
        if (meta.error && meta.error.stack) {
            err = meta;
        }

        if (err) {
            console.trace(err.stack);
        }
        else if (meta) {
            console.error(JSON.stringify(meta));
        }
    }

};

ConsoleTranspot.prototype.getOptionByEnv = function (env) {
    return this.options[env] || this.options.default;
};

function getLogDate() {
    var date = new Date();
    return date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear() + ' ' + date.toTimeString();
}

module.exports = ConsoleTranspot;